#!/bin/bash
########################################################################
#
# virt-install-cloud.sh: script to start an OpenStack cloud image on kvm
# version: 2.0
# This version works only with recent cloud-init releases so it might
#  not be compatible with old cloud cloud images
#  (use v1 of this script instead)
#
# Author: Claude Durocher
# License: Apache 2.0
#
# ref. http://mojodna.net/2014/05/14/kvm-libvirt-and-ubuntu-14-04.html
#      https://docs.openstack.org/image-guide/obtain-images.html
#      https://docs.openstack.org/nova/queens/user/config-drive.html
#
# requires the following packages on Ubuntu host:
#  wget qemu-kvm libvirt-bin virtinst bridge-utils genisoimage|xorriso
# requires the following packages on CentOS host:
#  wget qemu-kvm libvirt virt-install bridge-utils genisoimage|xorriso
#
########################################################################

# check if the script is run as root user
if [[ ${USER} != "root" ]]; then
  echo "This script must be run as root!" && exit 1
fi

BACKUP_ISO_FILES="no" # backup guest config files : yes or no

# image selection
IMG="bionic" # xenial, jessie, centos7, fedora26, ...
ARCH="amd64" # architecture : amd64 or i386
# virt-install settings for the guest
GUEST_NAME="bionic" # name of guest vm
DOMAIN="vm" # dns domain name of guest vm
VCPUS=2 # guest vcpus
VMEM=2048 # guest memory
NETWORK="bridge=virbr0,model=virtio" # debian jessie, use model=e1000 instead of virtio
RESIZE="yes" # resize root file system : yes or no
VROOTDISKSIZE="10G" # guest root file system size
FORMAT="qcow2" # guest image format: qcow2 or raw
CONVERT="no" # convert image to specified format : yes or no
POOL="vm" # kvm pool, default="default"
POOL_PATH="/home/vm" # kvm pool path, default="/var/lib/libvirt/images"

# cloud-init network settings
NIC="ens3" # xenial and newer, use NIC=ens3 instead of eth0
IPADDRESS="192.168.122.124"
IPNETWORK="192.168.122.0"
IPNETMASK="255.255.255.0"
IPBROADCAST="192.168.122.255"
IPGATEWAY="192.168.122.1"
NAMESERVER="192.168.122.1"

# cloud-init config_drive dirs
mkdir -p ${GUEST_NAME}/openstack/latest ${GUEST_NAME}/openstack/content

# main cloud-init file
cat <<EOF > ${GUEST_NAME}/openstack/latest/meta_data.json
{
  "files":[
    {
      "content_path":"/content/0000",
      "path":"/etc/foo.cfg"
    }
  ],
  "hostname":"${GUEST_NAME}.${DOMAIN}",
  "meta":{
    "dsmode":"local",
    "my-meta":"my-value"
  },
  "name":"${GUEST_NAME}",
  "uuid":"${GUEST_NAME}.${DOMAIN}"
}
EOF

# network config (rename network_data.json to *.bak if you want to use DHCP)
cat <<EOF > ${GUEST_NAME}/openstack/latest/network_data.json
{
  "links":[
    {
      "id":"interface_${NIC}",
      "name":"${NIC}",
      "type":"phy",
      "mtu":1500
    }
  ],
  "networks":[
    {
      "id":"publicnet-ipv4",
      "type":"ipv4",
      "link":"interface_${NIC}",
      "ip_address":"${IPADDRESS}",
      "netmask":"${IPNETMASK}",
      "dns_nameservers":[
        "${NAMESERVER}"
      ],
      "routes":[
        {
          "network":"0.0.0.0",
          "netmask":"0.0.0.0",
          "gateway":"${IPGATEWAY}"
        }
      ]
    }
  ],
  "services":[
    {
      "type":"dns",
      "address":"${NAMESERVER}"
    }
  ]
}
EOF

# instance config (ref. https://help.ubuntu.com/community/CloudInit)
cat <<EOF > ${GUEST_NAME}/openstack/latest/user_data
#cloud-config
password: password
chpasswd: { expire: False }
ssh_pwauth: True
# upgrade packages on startup
package_upgrade: false
#run 'apt-get upgrade' or yum equivalent on first boot
apt_upgrade: false
#manage_etc_hosts: localhost
manage_etc_hosts: true
fqdn: ${GUEST_NAME}.${DOMAIN}
datasource_list:
  - ConfigDrive
#  - NoCloud
# install additional packages
packages:
  - haveged
  - mc
  - htop
#  - language-pack-fr
# run commands
runcmd:
# install htop on centos/fedora
#  - [ sh, -c, "curl http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-2.noarch.rpm -o /tmp/epel-release.rpm" ]
#  - [ sh, -c, "yum install -y /tmp/epel-release.rpm" ]
#  - [ sh, -c, "yum install -y htop" ]
#ssh_authorized_keys:
#  - ssh-rsa AAAAB3NzaC1yc2QwAAADAQABAAa3BAQC0g+ZTxC7weoIJLUafOgrm+h...
EOF

# static files to copy to guest (see meta_data.json)
cat <<EOF > ${GUEST_NAME}/openstack/content/0000
# This is contents of /etc/foo.cfg
# please do something fun with it.
EOF

########################################################################
# don't edit below unless you know wat you're doing!

# download cloud image if not already downloaded
#  ref. https://openstack.redhat.com/Image_resources
case ${IMG} in
  trusty)
    IMG_USER="ubuntu"
    IMG_URL="http://cloud-images.ubuntu.com/releases/14.04/release"
    IMG_NAME="ubuntu-14.04-server-cloudimg-${ARCH}-disk1.img"
      ;;
  xenial)
    IMG_USER="ubuntu"
    IMG_URL="http://cloud-images.ubuntu.com/releases/16.04/release"
    IMG_NAME="ubuntu-16.04-server-cloudimg-${ARCH}-disk1.img"
    ;;
  bionic)
    IMG_USER="ubuntu"
    IMG_URL="http://cloud-images.ubuntu.com/releases/18.04/release"
    IMG_NAME="ubuntu-18.04-server-cloudimg-${ARCH}.img"
    ;;
  bionic-min)
    IMG_USER="ubuntu"
    IMG_URL="http://cloud-images.ubuntu.com/releases/18.04/release"
    IMG_NAME="ubuntu-18.04-minimal-cloudimg-${ARCH}.img"
    ;;
  centos6)
    IMG_USER="centos"
    IMG_URL="https://cloud.centos.org/centos/6/images"
    if [[ $ARCH = "amd64" ]]; then
      IMG_NAME="CentOS-6-x86_64-GenericCloud.qcow2"
    else
      echo "Cloud image not available!"; exit 1
    fi
    ;;
  centos7)
    IMG_USER="centos"
    IMG_URL="https://cloud.centos.org/centos/7/images"
    if [[ $ARCH = "amd64" ]]; then
      IMG_NAME="CentOS-7-x86_64-GenericCloud.qcow2"
    else
      echo "Cloud image not available!"; exit 1
    fi
    ;;
  fedora27)
    IMG_USER="fedora"
    if [[ $ARCH = "amd64" ]]; then
      IMG_URL="https://download.fedoraproject.org/pub/fedora/linux/releases/28/Cloud/x86_64/images/"
      IMG_NAME="Fedora-Cloud-Base-28-1.1.x86_64.qcow2"
    else
      echo "Cloud image not available!"; exit 1
    fi
    ;;
  jessie)
    # virtio nic don't work with this image
    IMG_USER="debian"
    if [[ $ARCH = "amd64" ]]; then
      IMG_URL="https://cdimage.debian.org/cdimage/openstack/current-8"
      IMG_NAME="debian-8-openstack-amd64.qcow2"
    else
      echo "Cloud image not available!"; exit 1
    fi
    ;;
  stretch)
    IMG_USER="debian"
    if [[ $ARCH = "amd64" ]]; then
      IMG_URL="https://cdimage.debian.org/cdimage/openstack/current-9"
      IMG_NAME="debian-9-openstack-amd64.qcow2"
    else
      echo "Cloud image not available!"; exit 1
    fi
    ;;
  buster)
    IMG_USER="debian"
    if [[ $ARCH = "amd64" ]]; then
      IMG_URL="https://cdimage.debian.org/cdimage/openstack/current-9"
      IMG_NAME="debian-9-openstack-amd64.qcow2"
    else
      echo "Cloud image not available!"; exit 1
    fi
    ;;
  *)
    echo "Cloud image not available!"; exit 1
    ;;
esac
if [[ ! -f ${IMG_NAME} ]]; then
  echo "Downloading image ${IMG_NAME}..."
  wget ${IMG_URL}/${IMG_NAME} -O ${IMG_NAME}
  chmod 666 ${IMG_NAME}
else
  echo "Using existing image ${IMG_NAME}..."
fi

# check if pool exists, otherwise create it
if [[ "$(virsh pool-list|grep ${POOL} -c)" -ne "1" ]]; then
  echo "Creating pool ${POOL}..."
  virsh pool-define-as --name ${POOL} --type dir --target ${POOL_PATH}
  virsh pool-autostart ${POOL}
  virsh pool-build ${POOL}
  virsh pool-start ${POOL}
fi

# write the cloud-init files into an ISO
echo "Preparing ISO file required by cloud-init..."
cd ${GUEST_NAME}
xorriso -in_charset utf8 -joliet on -rockridge on \
  -outdev config.iso -volid config-2 -add ./openstack
cd ..
# copy ISO into libvirt's directory
cp ${GUEST_NAME}/config.iso ${POOL_PATH}/${GUEST_NAME}.config.iso
chmod 664 ${POOL_PATH}/${GUEST_NAME}.config.iso
virsh pool-refresh ${POOL}

# copy image to libvirt's pool
if [[ ! -f ${POOL_PATH}/${IMG_NAME} ]]; then
  cp ${IMG_NAME} ${POOL_PATH}
  chmod 664 ${POOL_PATH}/${IMG_NAME}
  virsh pool-refresh ${POOL}
fi

# clone cloud image
virsh vol-clone --pool ${POOL} ${IMG_NAME} ${GUEST_NAME}.root.img
if [[ "${RESIZE}" == "yes" ]]; then
  virsh vol-resize --pool ${POOL} ${GUEST_NAME}.root.img ${VROOTDISKSIZE}
fi

# convert image format
if [[ "${CONVERT}" == "yes" ]]; then
  echo "Converting image to format ${FORMAT}..."
  qemu-img convert -O ${FORMAT} ${POOL_PATH}/${GUEST_NAME}.root.img ${POOL_PATH}/${GUEST_NAME}.root.img.${FORMAT}
  rm ${POOL_PATH}/${GUEST_NAME}.root.img
  mv ${POOL_PATH}/${GUEST_NAME}.root.img.${FORMAT} ${POOL_PATH}/${GUEST_NAME}.root.img
fi

chmod 664 ${POOL_PATH}/${GUEST_NAME}.root.img
echo
ls -lisa ${POOL_PATH}/ | grep ${GUEST_NAME}.*

echo "Creating guest ${GUEST_NAME}..."
virt-install \
  --name ${GUEST_NAME} \
  --ram ${VMEM} \
  --vcpus=${VCPUS} \
  --noautoconsole \
  --autostart \
  --memballoon virtio \
  --network ${NETWORK} \
  --boot hd \
  --disk vol=${POOL}/${GUEST_NAME}.root.img,format=${FORMAT},bus=virtio \
  --disk vol=${POOL}/${GUEST_NAME}.config.iso,device=cdrom

# display result
echo
echo "List of running VMs :"
echo
virsh list

# cleanup
if [[ "${BACKUP_ISO_FILES}" == "yes" ]]; then
  rm -R ${GUEST_NAME}.bkp
  mv ${GUEST_NAME} ${GUEST_NAME}.bkp
  find ${GUEST_NAME}.bkp -type d -exec chmod 755 {} \;
  find ${GUEST_NAME}.bkp -type f -exec chmod 644 {} \;
else
  rm -R ${GUEST_NAME}/
fi

# stuff to remember
echo
echo "************************"
echo "Useful stuff to remember"
echo "************************"
echo
echo "To login to vm guest:"
echo " sudo virsh console ${GUEST_NAME}"
echo "Default user for cloud image is :"
echo " ${IMG_USER}"
echo
echo "To edit guest vm config:"
echo " sudo virsh edit ${GUEST_NAME}"
echo
echo "To resize root volume (stop guest first):"
echo "   virsh vol-resize --pool ${POOL} ${GUEST_NAME}.root.img 20G"
echo
echo "To create a volume:"
echo " virsh vol-create-as ${POOL} ${GUEST_NAME}.vol1.img 20G --format ${FORMAT}"
echo "To attach a volume to an existing guest:"
echo " virsh attach-disk ${GUEST_NAME} --source ${POOL_PATH}/${GUEST_NAME}.vol1.img --target vdc --driver qemu --subdriver ${FORMAT} --persistent"
echo "To prepare the newly attached volume on guest:"
echo " sgdisk -n 1 -g /dev/vdc && && mkfs -t ext4 /dev/vdc1 && sgdisk -c 1:'vol1' -g /dev/vdc && sgdisk -p /dev/vdc"
echo " mkdir /mnt/vol1"
echo " echo '/dev/vdc1 /mnt/vol1 ext4 defaults,relatime 0 0' >> /etc/fstab"
echo
echo "To shutdown a guest vm:"
echo "  sudo virsh shutdown ${GUEST_NAME}"
echo

#sudo virsh console ${GUEST_NAME}
